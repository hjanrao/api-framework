$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("CarSelection.feature");
formatter.feature({
  "line": 1,
  "name": "Get car offer from auto1.com",
  "description": "",
  "id": "get-car-offer-from-auto1.com",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "As a customer, I want to select the type of car I want to sell, so that I can receive an offer from auto1.com",
  "description": "",
  "id": "get-car-offer-from-auto1.com;as-a-customer,-i-want-to-select-the-type-of-car-i-want-to-sell,-so-that-i-can-receive-an-offer-from-auto1.com",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@feature"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "feature is available to the user",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user selects manufacturer Bentley",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "selects main-type Continental GT",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "user retrieve built-dates by selected main-type",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "the status code is 200",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "response includes the following dates",
  "rows": [
    {
      "cells": [
        "2003",
        "2003"
      ],
      "line": 11
    },
    {
      "cells": [
        "2004",
        "2004"
      ],
      "line": 12
    },
    {
      "cells": [
        "2005",
        "2005"
      ],
      "line": 13
    },
    {
      "cells": [
        "2006",
        "2006"
      ],
      "line": 14
    },
    {
      "cells": [
        "2007",
        "2007"
      ],
      "line": 15
    },
    {
      "cells": [
        "2008",
        "2008"
      ],
      "line": 16
    },
    {
      "cells": [
        "2009",
        "2009"
      ],
      "line": 17
    },
    {
      "cells": [
        "2010",
        "2010"
      ],
      "line": 18
    },
    {
      "cells": [
        "2011",
        "2011"
      ],
      "line": 19
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "CarStepDefinitions.featureIsAvailableToUser()"
});
formatter.result({
  "duration": 928443646,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Bentley",
      "offset": 26
    }
  ],
  "location": "CarStepDefinitions.userSelectsManufacturer(String)"
});
formatter.result({
  "duration": 366561691,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Continental GT",
      "offset": 18
    }
  ],
  "location": "CarStepDefinitions.selectsMainType(String)"
});
formatter.result({
  "duration": 116947017,
  "status": "passed"
});
formatter.match({
  "location": "CarStepDefinitions.userRetrieveBuiltDatesBySelectedMainType()"
});
formatter.result({
  "duration": 112784150,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 19
    }
  ],
  "location": "CarStepDefinitions.verifyStatusCode(int)"
});
formatter.result({
  "duration": 559172,
  "status": "passed"
});
formatter.match({
  "location": "CarStepDefinitions.responseIncludesData(String,String\u003e)"
});
formatter.result({
  "duration": 89811008,
  "status": "passed"
});
formatter.scenario({
  "line": 22,
  "name": "As a user I want to make sure the API schema is correct for manufacturer service",
  "description": "",
  "id": "get-car-offer-from-auto1.com;as-a-user-i-want-to-make-sure-the-api-schema-is-correct-for-manufacturer-service",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 21,
      "name": "@schema"
    }
  ]
});
formatter.step({
  "line": 23,
  "name": "feature is available to the user",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "user selects manufacturer Bentley",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "the status code is 200",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "API schema for manufacturer should be correct",
  "keyword": "And "
});
formatter.match({
  "location": "CarStepDefinitions.featureIsAvailableToUser()"
});
formatter.result({
  "duration": 104617532,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Bentley",
      "offset": 26
    }
  ],
  "location": "CarStepDefinitions.userSelectsManufacturer(String)"
});
formatter.result({
  "duration": 121021575,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 19
    }
  ],
  "location": "CarStepDefinitions.verifyStatusCode(int)"
});
formatter.result({
  "duration": 86641,
  "status": "passed"
});
formatter.match({
  "location": "CarStepDefinitions.schemaForManufacturerShouldBeCorrect()"
});
formatter.result({
  "duration": 138566645,
  "status": "passed"
});
formatter.scenario({
  "line": 29,
  "name": "As a user I want to make sure the API schema is correct for main-type service",
  "description": "",
  "id": "get-car-offer-from-auto1.com;as-a-user-i-want-to-make-sure-the-api-schema-is-correct-for-main-type-service",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 28,
      "name": "@schema"
    }
  ]
});
formatter.step({
  "line": 30,
  "name": "feature is available to the user",
  "keyword": "Given "
});
formatter.step({
  "line": 31,
  "name": "user selects manufacturer Bentley",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "selects main-type Continental GT",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "the status code is 200",
  "keyword": "Then "
});
formatter.step({
  "line": 34,
  "name": "API schema for main-type should be correct",
  "keyword": "And "
});
formatter.match({
  "location": "CarStepDefinitions.featureIsAvailableToUser()"
});
formatter.result({
  "duration": 100084188,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Bentley",
      "offset": 26
    }
  ],
  "location": "CarStepDefinitions.userSelectsManufacturer(String)"
});
formatter.result({
  "duration": 119993867,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Continental GT",
      "offset": 18
    }
  ],
  "location": "CarStepDefinitions.selectsMainType(String)"
});
formatter.result({
  "duration": 113740955,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 19
    }
  ],
  "location": "CarStepDefinitions.verifyStatusCode(int)"
});
formatter.result({
  "duration": 68765,
  "status": "passed"
});
formatter.match({
  "location": "CarStepDefinitions.schemaForMainTypeShouldBeCorrect()"
});
formatter.result({
  "duration": 1597858,
  "status": "passed"
});
formatter.scenario({
  "line": 37,
  "name": "As a user I want to make sure the API schema is correct for built-dates service",
  "description": "",
  "id": "get-car-offer-from-auto1.com;as-a-user-i-want-to-make-sure-the-api-schema-is-correct-for-built-dates-service",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 36,
      "name": "@schema"
    }
  ]
});
formatter.step({
  "line": 38,
  "name": "feature is available to the user",
  "keyword": "Given "
});
formatter.step({
  "line": 39,
  "name": "user selects manufacturer Bentley",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "selects main-type Continental GT",
  "keyword": "And "
});
formatter.step({
  "line": 41,
  "name": "user retrieve built-dates by selected main-type",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "the status code is 200",
  "keyword": "Then "
});
formatter.step({
  "line": 43,
  "name": "API schema for built-dates should be correct",
  "keyword": "And "
});
formatter.match({
  "location": "CarStepDefinitions.featureIsAvailableToUser()"
});
formatter.result({
  "duration": 103111754,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Bentley",
      "offset": 26
    }
  ],
  "location": "CarStepDefinitions.userSelectsManufacturer(String)"
});
formatter.result({
  "duration": 113471979,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Continental GT",
      "offset": 18
    }
  ],
  "location": "CarStepDefinitions.selectsMainType(String)"
});
formatter.result({
  "duration": 114658672,
  "status": "passed"
});
formatter.match({
  "location": "CarStepDefinitions.userRetrieveBuiltDatesBySelectedMainType()"
});
formatter.result({
  "duration": 103503506,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 19
    }
  ],
  "location": "CarStepDefinitions.verifyStatusCode(int)"
});
formatter.result({
  "duration": 70750,
  "status": "passed"
});
formatter.match({
  "location": "CarStepDefinitions.schemaForBuiltDatesShouldBeCorrect()"
});
formatter.result({
  "duration": 1239370,
  "status": "passed"
});
formatter.scenario({
  "line": 46,
  "name": "User calls web service to get manufacturer with incorrect wa_key",
  "description": "",
  "id": "get-car-offer-from-auto1.com;user-calls-web-service-to-get-manufacturer-with-incorrect-wa-key",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 45,
      "name": "@error_codes"
    }
  ]
});
formatter.step({
  "line": 47,
  "name": "feature is available to the user",
  "keyword": "Given "
});
formatter.step({
  "line": 48,
  "name": "user selects Bentley manufacturer with invalid wa_key",
  "keyword": "When "
});
formatter.step({
  "line": 49,
  "name": "the status code is 403",
  "keyword": "Then "
});
formatter.match({
  "location": "CarStepDefinitions.featureIsAvailableToUser()"
});
formatter.result({
  "duration": 106463766,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Bentley",
      "offset": 13
    }
  ],
  "location": "CarStepDefinitions.userSelectsBentleyManufacturerWithInvalidWa_key(String)"
});
formatter.result({
  "duration": 116290182,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "403",
      "offset": 19
    }
  ],
  "location": "CarStepDefinitions.verifyStatusCode(int)"
});
formatter.result({
  "duration": 82999,
  "status": "passed"
});
formatter.scenario({
  "line": 52,
  "name": "User should not authorized without wa_key",
  "description": "",
  "id": "get-car-offer-from-auto1.com;user-should-not-authorized-without-wa-key",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 51,
      "name": "@error_codes"
    }
  ]
});
formatter.step({
  "line": 53,
  "name": "feature is available to the user",
  "keyword": "Given "
});
formatter.step({
  "line": 54,
  "name": "user selects Bentley manufacturer without wa_key",
  "keyword": "When "
});
formatter.step({
  "line": 55,
  "name": "the status code is 401",
  "keyword": "Then "
});
formatter.match({
  "location": "CarStepDefinitions.featureIsAvailableToUser()"
});
formatter.result({
  "duration": 116062731,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Bentley",
      "offset": 13
    }
  ],
  "location": "CarStepDefinitions.userSelectsBentleyManufacturerWithoutWa_key(String)"
});
formatter.result({
  "duration": 100100201,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "401",
      "offset": 19
    }
  ],
  "location": "CarStepDefinitions.verifyStatusCode(int)"
});
formatter.result({
  "duration": 82574,
  "status": "passed"
});
formatter.scenario({
  "line": 58,
  "name": "As a customer, I wont be allowed to create new record",
  "description": "",
  "id": "get-car-offer-from-auto1.com;as-a-customer,-i-wont-be-allowed-to-create-new-record",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 57,
      "name": "@error_codes"
    }
  ]
});
formatter.step({
  "line": 59,
  "name": "feature is available to the user",
  "keyword": "Given "
});
formatter.step({
  "line": 60,
  "name": "user try to create new record for manufacturer",
  "keyword": "When "
});
formatter.step({
  "line": 61,
  "name": "the status code is 405",
  "keyword": "Then "
});
formatter.match({
  "location": "CarStepDefinitions.featureIsAvailableToUser()"
});
formatter.result({
  "duration": 105613570,
  "status": "passed"
});
formatter.match({
  "location": "CarStepDefinitions.userTryToCreateNewRecordForManufacturer()"
});
formatter.result({
  "duration": 313206159,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "405",
      "offset": 19
    }
  ],
  "location": "CarStepDefinitions.verifyStatusCode(int)"
});
formatter.result({
  "duration": 101473,
  "status": "passed"
});
formatter.scenario({
  "line": 64,
  "name": "As a customer, I wont be able to delete existing record",
  "description": "",
  "id": "get-car-offer-from-auto1.com;as-a-customer,-i-wont-be-able-to-delete-existing-record",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 63,
      "name": "@error_codes"
    }
  ]
});
formatter.step({
  "line": 65,
  "name": "feature is available to the user",
  "keyword": "Given "
});
formatter.step({
  "line": 66,
  "name": "user try to delete Bentley manufacturer",
  "keyword": "When "
});
formatter.step({
  "line": 67,
  "name": "the status code is 403",
  "keyword": "Then "
});
formatter.match({
  "location": "CarStepDefinitions.featureIsAvailableToUser()"
});
formatter.result({
  "duration": 103494234,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Bentley",
      "offset": 19
    }
  ],
  "location": "CarStepDefinitions.userTryToDeleteBentleyManufacturer(String)"
});
formatter.result({
  "duration": 355949227,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "403",
      "offset": 19
    }
  ],
  "location": "CarStepDefinitions.verifyStatusCode(int)"
});
formatter.result({
  "duration": 129292,
  "status": "passed"
});
formatter.scenario({
  "line": 70,
  "name": "As a user I want to make sure the bad request is handled",
  "description": "",
  "id": "get-car-offer-from-auto1.com;as-a-user-i-want-to-make-sure-the-bad-request-is-handled",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 69,
      "name": "@error_codes"
    }
  ]
});
formatter.step({
  "line": 71,
  "name": "feature is available to the user",
  "keyword": "Given "
});
formatter.step({
  "line": 72,
  "name": "user selects manufacturer Bentley",
  "keyword": "When "
});
formatter.step({
  "line": 73,
  "name": "user sends bad request to fetch main-types",
  "keyword": "And "
});
formatter.step({
  "line": 74,
  "name": "the status code is 400",
  "keyword": "Then "
});
formatter.match({
  "location": "CarStepDefinitions.featureIsAvailableToUser()"
});
formatter.result({
  "duration": 326999691,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Bentley",
      "offset": 26
    }
  ],
  "location": "CarStepDefinitions.userSelectsManufacturer(String)"
});
formatter.result({
  "duration": 142999707,
  "status": "passed"
});
formatter.match({
  "location": "CarStepDefinitions.userSendsBadRequestToFetchMainTypes()"
});
formatter.result({
  "duration": 126914023,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "400",
      "offset": 19
    }
  ],
  "location": "CarStepDefinitions.verifyStatusCode(int)"
});
formatter.result({
  "duration": 112992,
  "status": "passed"
});
});