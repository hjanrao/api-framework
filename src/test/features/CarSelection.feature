Feature: Get car offer from auto1.com

  @feature
  Scenario: As a customer, I want to select the type of car I want to sell, so that I can receive an offer from auto1.com
    Given feature is available to the user
    When user selects manufacturer Bentley
    And selects main-type Continental GT
    And user retrieve built-dates by selected main-type
    Then the status code is 200
    And response includes the following dates
      | 2003 | 2003 |
      | 2004 | 2004 |
      | 2005 | 2005 |
      | 2006 | 2006 |
      | 2007 | 2007 |
      | 2008 | 2008 |
      | 2009 | 2009 |
      | 2010 | 2010 |
      | 2011 | 2011 |

  @schema
  Scenario: As a user I want to make sure the API schema is correct for manufacturer service
    Given feature is available to the user
    When user selects manufacturer Bentley
    Then the status code is 200
    And API schema for manufacturer should be correct

  @schema
  Scenario: As a user I want to make sure the API schema is correct for main-type service
    Given feature is available to the user
    When user selects manufacturer Bentley
    And selects main-type Continental GT
    Then the status code is 200
    And API schema for main-type should be correct

  @schema
  Scenario: As a user I want to make sure the API schema is correct for built-dates service
    Given feature is available to the user
    When user selects manufacturer Bentley
    And selects main-type Continental GT
    And user retrieve built-dates by selected main-type
    Then the status code is 200
    And API schema for built-dates should be correct

  @error_codes
  Scenario: User calls web service to get manufacturer with incorrect wa_key
    Given feature is available to the user
    When user selects Bentley manufacturer with invalid wa_key
    Then the status code is 403

  @error_codes
  Scenario: User should not authorized without wa_key
    Given feature is available to the user
    When user selects Bentley manufacturer without wa_key
    Then the status code is 401

  @error_codes
  Scenario: As a customer, I wont be allowed to create new record
    Given feature is available to the user
    When user try to create new record for manufacturer
    Then the status code is 405

  @error_codes
  Scenario: As a customer, I wont be able to delete existing record
    Given feature is available to the user
    When user try to delete Bentley manufacturer
    Then the status code is 403

  @error_codes
  Scenario: As a user I want to make sure the bad request is handled
    Given feature is available to the user
    When user selects manufacturer Bentley
    And user sends bad request to fetch main-types
    Then the status code is 400