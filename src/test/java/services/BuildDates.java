package services;

import constants.Services;
import io.restassured.response.Response;
import utils.PropertyUtils;
import utils.Utils;

public class BuildDates {

    public static Response getBuildDates(String manufacturer, String mainType) {

        return Utils
                .getHeader()
                .queryParam("wa_key", PropertyUtils.getsAppKey())
                .queryParam("manufacturer", manufacturer)
                .queryParam("main-type", mainType)
                .when()
                .get(Services.BUILT_DATES);
    }
}
