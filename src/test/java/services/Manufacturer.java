package services;

import constants.Services;
import io.restassured.response.Response;
import utils.PropertyUtils;
import utils.Utils;

public class Manufacturer {

    public static Response getManufacturer() {

        return Utils.getHeader()
                .queryParam("wa_key", PropertyUtils.getsAppKey())
                .when()
                .get(Services.MANUFACTURER);
    }

    public static Response getManufacturerWithInvalidKey() {

        return Utils.getHeader()
                .queryParam("wa_key", PropertyUtils.getsInvalidAppKey())
                .when()
                .get(Services.MANUFACTURER);
    }

    public static Response getManufacturerWithoutKey() {

        return Utils
                .getHeader()
                .when()
                .get(Services.MANUFACTURER);
    }

    public static Response postRequestManufacturer() {

        return Utils
                .getHeader()
                .queryParam("wa_key", PropertyUtils.getsAppKey())
                .body("'wkda' : {" +
                        "'Hemant' : 'Hemant'" +
                        "}")
                .when()
                .post(Services.MANUFACTURER);
    }

    public static Response deleteManufacturer(String key, String manufacturerName) {

        return Utils
                .getHeader()
                .queryParam("wa_key", PropertyUtils.getsAppKey())
                .body("\"wkda\" : {\n    \"" + key + "\" : \"" + manufacturerName + "\"\n    }")
                .when()
                .delete(Services.MANUFACTURER);
    }
}
