package services;

import constants.Services;
import io.restassured.response.Response;
import utils.PropertyUtils;
import utils.Utils;


public class MainTypes {

    public static Response getMainTypes(String manufacturer) {

        return Utils
                .getHeader()
                .queryParam("wa_key", PropertyUtils.getsAppKey())
                .queryParam("manufacturer", manufacturer)
                .when()
                .get(Services.MAIN_TYPES);
    }

    public static Response getMainTypesBadRequestWithoutManufacturer() {

        return Utils
                .getHeader()
                .queryParam("wa_key", PropertyUtils.getsAppKey())
                .when()
                .get(Services.MAIN_TYPES);
    }
}
