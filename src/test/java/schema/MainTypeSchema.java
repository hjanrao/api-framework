package schema;

import com.fasterxml.jackson.databind.node.ObjectNode;

public class MainTypeSchema {

    private int page;
    private long pageSize;
    private int totalPageCount;
    private ObjectNode wkda;

    public int getPage() {
        return this.page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public long getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPageCount() {
        return this.totalPageCount;
    }

    public void setTotalPageCount(int totalPageCount) {
        this.totalPageCount = totalPageCount;
    }

    public ObjectNode getWkda() {
        return this.wkda;
    }

    public void setWkda(ObjectNode wkda) {
        this.wkda = wkda;
    }
}
