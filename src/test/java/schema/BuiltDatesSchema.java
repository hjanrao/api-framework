package schema;

import com.fasterxml.jackson.databind.node.ObjectNode;

public class BuiltDatesSchema {

    private ObjectNode wkda;

    public ObjectNode getWkda() {
        return this.wkda;
    }


    public void setWkda(ObjectNode wkda) {
        this.wkda = wkda;
    }
}
