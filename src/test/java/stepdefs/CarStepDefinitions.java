package stepdefs;

import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import schema.BuiltDatesSchema;
import schema.MainTypeSchema;
import schema.ManufacturerSchema;
import services.BuildDates;
import services.MainTypes;
import services.Manufacturer;
import utils.Utils;

import java.io.IOException;
import java.util.Map;

public class CarStepDefinitions {

    private Response response;

    private String MANUFACTURER = null;
    private String MAIN_TYPES = null;

    @Given("feature is available to the user")
    public void featureIsAvailableToUser() {

        Assert.assertEquals(Manufacturer.getManufacturer().getStatusCode(), 200);
    }

    @When("user selects manufacturer (.*)")
    public void userSelectsManufacturer(String manufacturerName) {

        response = Manufacturer.getManufacturer();

        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertFalse(response.jsonPath().getMap("wkda").isEmpty());
        JsonPath manufacturerResponse = response.jsonPath();

        MANUFACTURER = Utils.getKey(manufacturerResponse.getMap("wkda"), manufacturerName);

    }

    @And("selects main-type (.*)")
    public void selectsMainType(String mainType) {

        response = MainTypes.getMainTypes(MANUFACTURER);

        Assert.assertEquals(response.getStatusCode(), 200);
        Assert.assertFalse(response.jsonPath().getMap("wkda").isEmpty());
        JsonPath mainTypeResponse = response.jsonPath();

        MAIN_TYPES = (String) mainTypeResponse.getMap("wkda").get(mainType);
    }

    @And("user retrieve built-dates by selected main-type")
    public void userRetrieveBuiltDatesBySelectedMainType() {

        response = BuildDates.getBuildDates(MANUFACTURER, MAIN_TYPES);

        Assert.assertFalse(response.jsonPath().getMap("wkda").isEmpty());
    }

    @Then("the status code is (\\d+)")
    public void verifyStatusCode(int statusCode) {

        Assert.assertEquals(statusCode, response.getStatusCode());
    }

    @And("response includes the following dates$")
    public void responseIncludesData(Map<String, String> responseFields) {
        for (Map.Entry<String, String> field : responseFields.entrySet()) {
            if (StringUtils.isAlphanumeric(field.getValue())) {
                Assert.assertTrue(response.jsonPath().getMap("wkda").containsKey(field.getKey()));
                Assert.assertTrue(response.jsonPath().getMap("wkda").containsValue(field.getValue()));
            }
        }
    }

    @When("^user try to create new record for manufacturer$")
    public void userTryToCreateNewRecordForManufacturer() {
        response = Manufacturer.postRequestManufacturer();
    }

    @When("^user try to delete (.*) manufacturer$")
    public void userTryToDeleteBentleyManufacturer(String manufacturerName) {
        Response manufacturer = Manufacturer.getManufacturer();

        Assert.assertEquals(manufacturer.getStatusCode(), 200);

        response = Manufacturer.deleteManufacturer(Utils.getKey(manufacturer.jsonPath().getMap("wkda"),
                manufacturerName), manufacturerName);
    }

    @Then("^API schema for manufacturer should be correct$")
    public void schemaForManufacturerShouldBeCorrect() throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        ManufacturerSchema contract = mapper.readValue(response.asString(), ManufacturerSchema.class);

        Assert.assertNotNull(contract.getPage());
        Assert.assertTrue(Integer.class.isInstance(contract.getPage()));

        Assert.assertNotNull(contract.getPageSize());
        Assert.assertTrue(Long.class.isInstance(contract.getPageSize()));

        Assert.assertNotNull(contract.getTotalPageCount());
        Assert.assertTrue(Integer.class.isInstance(contract.getTotalPageCount()));

        Assert.assertNotNull(contract.getWkda());
    }

    @Then("^API schema for main-type should be correct$")
    public void schemaForMainTypeShouldBeCorrect() throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        MainTypeSchema contract = mapper.readValue(response.asString(), MainTypeSchema.class);

        Assert.assertNotNull(contract.getPage());
        Assert.assertTrue(Integer.class.isInstance(contract.getPage()));

        Assert.assertNotNull(contract.getPageSize());
        Assert.assertTrue(Long.class.isInstance(contract.getPageSize()));

        Assert.assertNotNull(contract.getTotalPageCount());
        Assert.assertTrue(Integer.class.isInstance(contract.getTotalPageCount()));

        Assert.assertNotNull(contract.getWkda());

    }

    @Then("^API schema for built-dates should be correct$")
    public void schemaForBuiltDatesShouldBeCorrect() throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        BuiltDatesSchema contract = mapper.readValue(response.asString(), BuiltDatesSchema.class);

        Assert.assertNotNull(contract.getWkda());
    }

    @When("^user selects (.*) manufacturer with invalid wa_key$")
    public void userSelectsBentleyManufacturerWithInvalidWa_key(String manufacturer) {
        response = Manufacturer.getManufacturerWithInvalidKey();
    }

    @When("^user selects (.*) manufacturer without wa_key$")
    public void userSelectsBentleyManufacturerWithoutWa_key(String manufacturer) {
        response = Manufacturer.getManufacturerWithoutKey();
    }

    @And("^user sends bad request to fetch main-types$")
    public void userSendsBadRequestToFetchMainTypes() {
        response = MainTypes.getMainTypesBadRequestWithoutManufacturer();
    }
}


