package utils;

import io.restassured.specification.RequestSpecification;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class Utils {

    public static String getKey(Map<Object, Object> map, String value) {
        for (Map.Entry<Object, Object> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey().toString();
            }
        }
        return null;
    }

    public static RequestSpecification getHeader() {
        return given()
                .baseUri(PropertyUtils.getsAppUrl())
                .basePath(PropertyUtils.getsEndPoint())
                .header("accept-language", PropertyUtils.getsAppLanguage());
    }

}
