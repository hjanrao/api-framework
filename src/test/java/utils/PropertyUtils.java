package utils;

import org.testng.log4testng.Logger;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyUtils {

    private static final String PROPERTY_FILE_NAME = "config.properties";
    private static Logger log = Logger.getLogger(PropertyUtils.class);
    private static final Properties PROPERTIES = getProperties();

    private static synchronized Properties getProperties() {
        try {
            InputStream inputStream = new BufferedInputStream(new FileInputStream(PROPERTY_FILE_NAME));
            Properties properties = new Properties();
            properties.load(inputStream);
            return properties;
        } catch (IOException e) {

            log.warn("Error reading the properties file", e);
            throw new RuntimeException("Error loading the properties file " + e);
        }
    }

    static String getsAppUrl() {

        return System.getProperty("APP_URL", PROPERTIES.getProperty("APP_URL"));
    }

    public static String getsAppKey() {
        return System.getProperty("APP_KEY", PROPERTIES.getProperty("APP_KEY"));
    }

    public static String getsInvalidAppKey() {
        return System.getProperty("INVALID_APP_KEY", PROPERTIES.getProperty("INVALID_APP_KEY"));
    }

    public static String getsEndPoint() {
        return System.getProperty("END_POINT", PROPERTIES.getProperty("END_POINT"));
    }

    static String getsAppLanguage() {
        return System.getProperty("ACCEPT_LANGUAGE", PROPERTIES.getProperty("ACCEPT_LANGUAGE"));
    }
}
