package constants;

public interface Services {

    String MANUFACTURER = "manufacturer";
    String MAIN_TYPES = "main-types";
    String BUILT_DATES = "built-dates";
}
