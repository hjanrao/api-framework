Tech-Stack used
---------------
* Rest-Assured
* Cucumber
* TestNG
* Maven

How to run tests
----------------
* run all: tests
        
        mvn test

* with tags:
        
        mvn test -Dcucumber.options="--tags @schema"
  
  tags : schema, feature, error_codes

Where to check reports
----------------------
Reports will be generated on below path 
RestAssignment/target/cucumber-reports/advanced-reports/cucumber-html-reports/
* report-feature_CarSelection-feature.html
* overview-features.html

Found below observations while testing the api:
-----------------------------------------------
#### Observation 
1. Key should be passed in header not as a query parameter.
2. As shown in the picture there is no API or option to send the data. i.e. no POST url is given so user can send 
   his/her data to auto1.com as of now user can only select the date from dropdown 
3. Call does’t fails when manufacturer/main-type is given wrong and it returns empty response
   e.g. http://api-aws-eu-qa-1.auto1-test.com/v1/car-types/main-types?wa_key=coding-puzzle-client-449cc9d&manufacturer=
4. When wrong url is hit it should be 404 but it gives 200 and empty array in body 
http://api-aws-eu-qa-1.auto1-test.com/v1/car-types/manufacturer/wrong-service?wa_key=coding-puzzle-client-449cc9d
5. Protocol used is http it should be https
6. Can test api against various load to test stress point and performance [Suggestion]
7. page size field is constant, it should be the correct value of the number of items in wkda array [Long data type]
8. In case of POST http://api-aws-eu-qa-1.auto1-test.com/v1/car-types/manufacturer?wa_key=coding-puzzle-client-449cc9d
    call its giving the trace details which can be vulnerable for security.
    Conclusion: ***POST call for all the three endpoint gives trace which may be a security issue if the exception 
    stacktrace returned from server***

